from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404, render, redirect
from django.http.response import JsonResponse
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponse
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from .models import Recipe
from .forms import RecipeForm
from datetime import datetime

@login_required(login_url='/admin/login/')  
def index(request):
    recipes = Recipe.objects.all().filter(user=request.user).values().order_by('date_added')
    recipe_count = len(recipes)
    form = RecipeForm(request.POST)
    if request.method == "POST" and form.is_valid():
        form = form.save(commit=False)
        form.user = request.user
        form.save()
        return redirect('/recipe-manager')

    response = {'recipes': recipes, 'recipe_count': recipe_count,'form': form, 'user':request.user}
    return render(request, 'recipe_index.html', response)



def delete_recipe(request, id):
    user = request.user
    if not user.is_authenticated and "Authorization" in request.headers:
        token = request.headers["Authorization"].split()[1]
        user = User.objects.get(auth_token = token)
        if not user :
            return JsonResponse({"error" : "no user can be found"},content_type='application/json',status=401)
    elif not user.is_authenticated :
        return JsonResponse({"error" : "user is not logged in"},content_type='application/json',status=401)
        
    recipe = get_object_or_404(Recipe, id = id)
    recipe.delete()
    return redirect("/recipe-manager")



@login_required(login_url='/admin/login/')
def filter_recipes(request, difficulty):
    if difficulty == "All":
        data = serializers.serialize('json', Recipe.objects.all().filter(user=request.user))
    else:
        data = serializers.serialize('json', Recipe.objects.all().filter(user=request.user).filter(difficulty=difficulty))
    return HttpResponse(data, content_type="application/json")



@login_required(login_url='/admin/login/')
def search_recipes(request, query):
    data = serializers.serialize('json', Recipe.objects.all().filter(user=request.user).filter(dish_name__icontains=query))
    return HttpResponse(data, content_type="application/json")

def get_recipes(request):
    user = request.user
    if not user.is_authenticated and "Authorization" in request.headers:
        token = request.headers["Authorization"].split()[1]
        user = User.objects.get(auth_token = token)
        if not user :
            return JsonResponse({"error" : "no user can be found"},content_type='application/json',status=401)
    elif not user.is_authenticated :
        return JsonResponse({"error" : "user is not logged in"},content_type='application/json',status=401)

    return HttpResponse(serializers.serialize('json', Recipe.objects.all().filter(user=user).order_by('date_added')), content_type="application/json")

@csrf_exempt
def add_recipe(request):
    form = RecipeForm(request.POST or None)
    data = {}

    if request.method == "POST" and "Authorization" in request.headers :
        token = request.headers['Authorization'].split()[1]
        user = User.objects.get(auth_token = token)
        if not user :
            return JsonResponse({"error" : "no user can be found"},content_type='application/json',status=401)
        name = request.POST.get("dish_name")
        recipe_link = request.POST.get("link")
        difficulty = request.POST.get("difficulty")
        date_added = datetime.strptime(request.POST.get("date_added"), '%Y-%m-%d %H:%M:%S.%f')

        recipe = Recipe(
            user = user,
            dish_name = name,
            link = recipe_link,
            difficulty = difficulty,
            date_added = date_added,
        )

        recipe.save()
        data['status'] = 'ok'
        return JsonResponse(data,status=200)
        
    else :
        return JsonResponse({"error": form.errors },status=400)