from django.urls import path
from .views import index, delete_recipe, filter_recipes, search_recipes, get_recipes, add_recipe

urlpatterns = [
    path('', index, name='index'),
    path('delete-recipe/<int:id>', delete_recipe, name='delete_recipe'),
    path('search/<str:query>', search_recipes, name='search_recipes'),
    path('filter/<str:difficulty>', filter_recipes, name='filter_recipes'),
    path('recipes', get_recipes, name='get_recipes'),
    path('add-recipe', add_recipe, name='add_recipe'),
    
]   