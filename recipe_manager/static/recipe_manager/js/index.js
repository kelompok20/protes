var filterbttn = document.getElementById("filter-button");
var searchbttn = document.getElementById("search-button");
var recipeTable = document.getElementById("recipe-table").firstElementChild;
console.log("hello");

filterbttn.addEventListener("click", function(){
    var diff = document.getElementById("difficulty").value;
    var ourRequest = new XMLHttpRequest();
    ourRequest.open('GET', diff);

    ourRequest.onload = function() {
        if (ourRequest.status >= 200 && ourRequest.status < 400) {
          var ourData = JSON.parse(ourRequest.responseText);
          renderHTML(ourData);
        } else {
          console.log("We connected to the server, but it returned an error.");
        }
        
    };

    ourRequest.onerror = function() {
        console.log("Connection error");
    };
    
    ourRequest.send();
});

searchbttn.addEventListener("click", function(){
    var query = document.getElementById("query").value;
    var ourRequest = new XMLHttpRequest();
    ourRequest.open('GET', "search/"+query);

    ourRequest.onload = function() {
        if (ourRequest.status >= 200 && ourRequest.status < 400) {
          var ourData = JSON.parse(ourRequest.responseText);
          renderHTML(ourData);
        } else {
          console.log("We connected to the server, but it returned an error.");
        }
        
    };

    ourRequest.onerror = function() {
        console.log("Connection error");
    };
    
    ourRequest.send();
});

function renderHTML(data) {
    var htmlString = "";
    const months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
  
    for (i = 0; i < data.length; i++) {
      var date = new Date(data[i].fields.date_added);
      htmlString += "<tr>";
      htmlString +="<td><a href='"+data[i].fields.link+"'>"+data[i].fields.dish_name+"</a></td>";
      htmlString +="<td>"+data[i].fields.difficulty+"</td>";
      htmlString +="<td>"+months[date.getMonth()]+" "+date.getDate()+", "+date.getFullYear()+"</td>";
      htmlString +="<td><i class='fas fa-pen'></i>\n<i class='far fa-trash-alt' name='delete' id='delete' value='"+ "delete-recipe/"+ data[i].pk +"'></i></td>"
      htmlString += '</tr>';
  
    }
    while (recipeTable.childElementCount>1) {
        recipeTable.removeChild(recipeTable.lastElementChild);
    }
    recipeTable.insertAdjacentHTML('beforeend', htmlString);
    var deletes = document.getElementsByName("delete");
    deletes.forEach(function(del){ del.addEventListener("click", func); });
}

document.getElementById('add').addEventListener("click", function() {
    document.getElementById('delete-prompt').style.display = "none";
	document.querySelector('.bg-modal').style.display = "block";
    document.getElementById('add-form').style.display = "block";
});

document.querySelector('.close').addEventListener("click", function() {
	document.querySelector('.bg-modal').style.display = "none";
});

var deletes = document.getElementsByName("delete");
deletes.forEach(function(del){
    del.addEventListener("click", func);
});

function func(){
    document.getElementById('add-form').style.display = "none";
        document.getElementById('delete-prompt').style.display = "block";
        document.getElementById('delete-prompt').setAttribute("value", this.getAttribute("value"));
        document.querySelector('.bg-modal').style.display = "block";
}

document.getElementById("delete-recipe").addEventListener("click", function() {
    var url = document.getElementById("delete-prompt").getAttribute("value");
    window.location.replace(url);
});
