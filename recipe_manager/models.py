from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

DIFFICULTY_CHOICES = (
    ('Easy', 'Easy'), 
    ('Medium', 'Medium'), 
    ('Hard', 'Hard')
    )

class Recipe(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    dish_name = models.CharField(max_length=100)
    link = models.URLField()
    difficulty = models.CharField(max_length=6, choices=DIFFICULTY_CHOICES, default='Easy')
    date_added = models.DateTimeField(default=timezone.now)

    
