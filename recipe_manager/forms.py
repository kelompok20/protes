from django import forms
from .models import Recipe

class RecipeForm(forms.ModelForm):
    class Meta:
        model = Recipe
        fields = [
            "dish_name",
            "link",
            "difficulty",
        ]
        widgets = {
            'dish_name' : forms.widgets.TextInput(attrs={'placeholder':'Name of the recipe'}),
            'difficulty': forms.widgets.Select(attrs={'class':'difficulty'}),
        }

    