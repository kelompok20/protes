# Generated by Django 3.2.8 on 2021-11-05 04:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('journal', '0002_auto_20211028_1817'),
    ]

    operations = [
        migrations.AlterField(
            model_name='journal',
            name='journal_title',
            field=models.CharField(default='', max_length=50),
        ),
    ]
