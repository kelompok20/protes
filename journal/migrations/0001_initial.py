# Generated by Django 3.2.8 on 2021-10-28 11:16

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Journal',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('journal_title', models.CharField(default='', max_length=100)),
                ('journal_entry', models.TextField(default='Masukkan isi jurnal Anda disini', max_length=2000)),
                ('quote_OTD', models.TextField(default="What's the quote of the day today?", max_length=500)),
                ('date_created', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
    ]
