from django.db import models
from django.utils import timezone

class Journal(models.Model):
    journal_title = models.CharField(max_length=50, default="")
    journal_entry = models.TextField(max_length=2000, default="")
    quote_OTD = models.TextField(max_length=500, default="")
    date_created = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.journal_title

# Create your models here.
