$(document).ready(function (){
    $(".js-add-journal").click(function(){
        $.ajax({
            url: 'add-entry',
            type: 'get',
            dataType: 'json',
            beforeSend: function(){
                   $("#modal-journal").modal("show");
            },
            success: function(data){
                $("#modal-journal .modal-content").html(data.html_form);
            }    
        });

    });
});

$(document).ready(function (){
    $("#modal-journal").on("submit", ".js-journal-form", function () {
        var form = $(this);
        $.ajax({
          url: form.attr("action"),
          data: form.serialize(),
          type: form.attr("method"),
          dataType: 'json',
          success: function (data) {
              alert("Entry created! Refresh to see your new entry.");
              $("#body").html("");
              $("#body").html(data.html_book_list);
              $("#modal-journal").modal("hide"); 
          },
          error: function (data){ 
            $("#modal-journal .modal-content").html(data.html_form);
           }
        });
        
        return false;
      });
});

