from django import forms
from django.forms.widgets import TextInput, Textarea
from .models import Journal

class JournalForm(forms.ModelForm):
    class Meta:
        model = Journal
        fields = ["journal_title", "journal_entry",
        "quote_OTD"]
        widgets = {"journal_title": TextInput(attrs={'placeholder': "Journal title here"}),
            "journal_entry": Textarea(attrs={'placeholder':"Journal entry here"}),
            "quote_OTD": Textarea(attrs={"placeholder": "What's today's quote of the day?"})}