from django.urls import path
from django.contrib.auth.decorators import login_required
from .views import *

urlpatterns = [
    path('', index, name='index'),
    path('add-entry', add_journal, name='add-entry'),
    path('journal-detail/<int:pk>', login_required(Detail_View.as_view()), name='journal-detail'),
    path('journal-detail/<int:pk>/update', login_required(Update_Journal.as_view()), name="journal-update"),
    path('journal-detail/<int:pk>/delete', delete_journal, name="journal-delete"),
    path('post-journal-entries', post_journal_entry, name="post-journal"),
    path('delete-journal-entries/<int:pk>', delete_journal_entry, name="delete-journal-entry"),
    path('get-journal-entries', get_journal_entry, name="get-entries")
]
