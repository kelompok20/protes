from django.http.response import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render, get_object_or_404
from django.template.loader import render_to_string
from .models import Journal
from .forms import JournalForm
from django.views.generic import DetailView, UpdateView
from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers

@login_required(login_url='admin/login')
def index(request):
    journals = Journal.objects.all().values().order_by("-date_created")
    response = {"journals": journals}
    return render(request, 'home.html', response)


class Detail_View(DetailView):
    model = Journal

@login_required(login_url='admin/login')
def add_journal(request):
    data = dict()
    form = JournalForm()
    if request.method == "POST":
        form = JournalForm(request.POST or None)
        if form.is_valid():
            form.save()
            data["form_is_valid"] = True
            journal = Journal.objects.all()
            data["html_journalist"] = render_to_string('journal_list_snippet.html', {'journal':journal})
        else:
            data["form_is_valid"] = False
    else:
        JournalForm()
    response = {'form': form}
    data['html_form'] = render_to_string('journal_form.html', response, request=request)
    return JsonResponse(data)


class Update_Journal(UpdateView):
    model = Journal
    fields = ["journal_title", "journal_entry",
        "quote_OTD"]
    template_name = 'journal_update_form.html'

    def get_success_url(self):
        return reverse_lazy("index")

@login_required(login_url='admin/login')
def delete_journal(request, pk):
    journal = get_object_or_404(Journal, pk=pk)
    if request.method == "POST":
        journal.delete()
        return HttpResponseRedirect("/")    
    response = {'journal': journal}
    return render(request, 'journal_confirm_delete.html', response)

@csrf_exempt
def post_journal_entry(request):
    form = JournalForm(request.POST or None)
    data = {}
    user = request.user

    if request.method == "POST" and "Authorization" in request.headers:
        token = request.headers['Authorization'].split()[1]
        user = User.objects.get(auth_token = token)
        if not user :
            return JsonResponse({"error" : "no user can be found"},content_type='application/json',status=401)
        title = request.POST.get("title")
        description = request.POST.get("description")
        quote = request.POST.get("quote")
        date_created = request.POST.get("date_created")
        status = request.POST.get("status")
        

        new_entry = Journal(
            user = user,
            title = title,
            description = description,
            quote = quote,
            date_created = date_created,
            status = status
        )
        new_entry.save()
        data['status'] = 'ok'
        return JsonResponse(data,status=200)

    elif user.is_authenticated and request.is_ajax():
        if form.is_valid():
            form = form.save(commit=False)
            form.user = user
            form.save()
            data['status'] = 'ok'
            return JsonResponse(data,status=200)
        else:
            return JsonResponse({"error":form.errors},status=400)
    
    return JsonResponse({"error" : "user is not logged in"},status=404)

@csrf_exempt
def update_journal_entry(request, id):
    response = {}
    journal_entries = get_object_or_404(Journal,id=id)

    if request.method == "POST" and "Authorization" in request.headers :
        token = request.headers['Authorization'].split()[1]
        user = User.objects.get(auth_token = token)

        if not user :
            return JsonResponse({"error" : "no user can be found"},content_type='application/json',status=401)

        title = request.POST.get("title")
        description = request.POST.get("description")
        quote = request.POST.get("quote")
        status = request.POST.get("status")

        update_entry = Journal.objects.get(user=user,pk=id)

        if update_entry is None:
            return JsonResponse({"error" : "object does not exist"},status=400)

        update_entry.title = title
        update_entry.description = description
        update_entry.quote = quote
        update_entry.status = status

        update_entry.save()
        response['status'] = 'ok'
        return JsonResponse(response,status=200)
    
    elif request.method == "POST" and request.is_ajax():
        form = Journal(request.POST,instance=journal_entries)
        if form.is_valid():
            form = form.save(commit=False)
            form.user = request.user
            form.save()
            response['status'] = 'ok'
            return JsonResponse(response,status=200)
        else:
            return JsonResponse({"error":form.errors},status=400)
    
    else:
        form = JournalForm()
    
    serialized = serializers.serialize('json',[journal_entries])
    return HttpResponse(serialized,content_type='application/json')

@csrf_exempt
def delete_journal_entry(request, id):
    response = {}
    journal_entry = get_object_or_404(Journal,id=id)
    print("delete")
    if request.method == "POST":
        journal_entry.delete()
        response = {'respon': f"entry with the id {id} has been deleted."}
        return JsonResponse(response,content_type='application/json',status=200)

@csrf_exempt
def get_journal_entry(request, id):
    user = request.user
    if not user.is_authenticated and "Authorization" in request.headers:
        token = request.headers["Authorization"].split()[1]
        user = User.objects.get(auth_token = token)
        if not user :
            return JsonResponse({"error" : "no user can be found"},content_type='application/json',status=401)
    elif not user.is_authenticated :
            return JsonResponse({"error" : "user is not logged in"},content_type='application/json',status=401)

    journal_query = [*Journal.objects.filter(day=str(id),user=user), *Journal.objects.all().filter(user=user)]
    serialized_query = serializers.serialize('json',journal_query)
    return HttpResponse(serialized_query,content_type='application/json')





    
