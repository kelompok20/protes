from django.urls import reverse
from .models import UserToken
from django.http.response import JsonResponse
from django.shortcuts import redirect, render
from django.contrib.auth import authenticate,login
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import logout as django_logout

def home(request):
    if request.user.is_authenticated:
        return redirect('tm-index')        
    return render(request, 'main/home.html')

@csrf_exempt
def login_page(request):
    if request.user.is_authenticated:
        return redirect('tm-index')      

    response = {}
    if request.method == "POST":
        username = request.POST.get("username")
        password = request.POST.get("password")
        user = authenticate(username=username,password=password)

        if user is not None:
            if not hasattr(user,'auth_token'):
                UserToken.objects.create(user=user)
            auth_token = user.auth_token
            login(request,user)
            response['auth_token'] = auth_token.key
            return redirect('tm-index')
        
        response['error'] = True
        return render(request,'main/login.html' ,response,status=400)

    return render(request,'main/login.html')

@csrf_exempt
def get_user_token(request):
    response = {}
    username = request.POST.get("username")
    password = request.POST.get("password")
    user = authenticate(username=username,password=password)
    if user is not None:
        if not hasattr(user,'auth_token'):
            UserToken.objects.create(user=user)
        auth_token = user.auth_token
        response['auth_token'] = auth_token.key
        return JsonResponse(response,content_type='application/json',status=200)
        
    response['error'] = f"{username} : {password}"
    return JsonResponse(response,content_type='application/json',status=400)

def logout(request):
    django_logout(request)
    return redirect('main:home')
