from django.urls import path
from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('login/',views.login_page,name='login'),
    path('logout/',views.logout,name='logout'),
    path('api-auth-token/',views.get_user_token,name='auth_token')
]
