from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import post_save
from django.dispatch import receiver
import binascii
import os

# Create your models here.

class UserToken(models.Model):
    key = models.CharField(max_length=40,primary_key=True)
    user =  models.OneToOneField(
        User,related_name='auth_token',
        on_delete= models.CASCADE,
        verbose_name=("User")
    )
    created = models.DateTimeField(_("Created"),auto_now_add=True)

    class Meta:
        verbose_name = _("Token")
        verbose_name_plural = _("Tokens")
    
    def save(self,*args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
        return super(UserToken,self).save(*args, **kwargs)

    def generate_key(self):
        return binascii.hexlify(os.urandom(20)).decode()
    
    @receiver(post_save, sender=User)
    def create_user_usertoken(sender, instance, created, **kwargs):
        if created:
            UserToken.objects.create(user=instance)

    @receiver(post_save, sender=User)
    def save_user_usertoken(sender, instance, **kwargs):
        instance.auth_token.save()

    def __str__(self):
        return self.key