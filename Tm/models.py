from django.db import models
from django.utils.translation import gettext_lazy as _
from datetime import date,datetime,time
from django.contrib.auth.models import User


class Task(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    judul = models.CharField(max_length=30)
    description = models.TextField()
    STATUS =  [
        ('1', _("On Going")),
        ('2', _("Completed")),
        ('3', _("Unfinished")),
    ]

    status = models.CharField(
        max_length=13,
        choices=STATUS,
        default= "1"
    )

    def time_in_range(self,start, end, now):
        """Return true if x is in the range [start, end]"""
        if start <= end:
            return start <= now <= end
        else:
            return start <= now or now <= end

    def clean(self):
        if hasattr(self,'deadlines'):
            if self.status != "2" and self.deadlines < date.today():
                self.status = "3"

        elif hasattr(self,'start_time') and hasattr(self,'end_time'):
            now = datetime.now()
            current_time = time(now.hour,now.minute,now.second)
            if self.status != "2" and not self.time_in_range(self.start_time,self.end_time,current_time) and  current_time > self.start_time:
                self.status = "3"

    def __str__(self) -> str:
        return self.judul

    

class TargetedTask(Task):
    deadlines = models.DateField(null=True)

class DailyTask(Task):

    WEEKDAYS = [
        ('1', _("Monday")),
        ('2', _("Tuesday")),
        ('3', _("Wednesday")),
        ('4', _("Thursday")),
        ('5', _("Friday")),
        ('6', _("Saturday")),
        ('0', _("Sunday")),
    ]
        
    day = models.CharField(
        max_length= 9,
        choices = WEEKDAYS
    )

    start_time = models.TimeField(null=True) 
    end_time = models.TimeField(null=True)
