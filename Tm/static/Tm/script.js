let date = new Date();
let sortStatusTasks = [];

let fetchAlltasks = (renderTasks, status) => {
  let link = "get_tasks/";
  $.ajax({
    url: link,
    success: (response) => renderTasks(response, status),
    error: (response) => {
      console.log(response);
    },
  });
};

let fetchDailyTasks = (renderDailyTasks) => {
  let dates = new Date().getDay();
  let link = `get_daily_tasks/${dates}`;
  $.ajax({
    url: link,
    success: (response) => renderDailyTasks(response),
    error: (response) => console.log(response),
  });
};

let remainingTasks = () => {
  $.ajax({
    url: 'remaining-tasks/',
    success : (response) => {
      $('.number-task').html(response.counts)
    },
    error: (response) => console.log(response)
  })
}

let renderDailyTasks = (response) => {

    $(document).ready(() => {
    let nowDate = new Date();
    let nowHour = nowDate.getHours();
    let nowMin = nowDate.getMinutes(); 
    let sortedDailyTask = sortDaily(response);
    for (let i = 0; i < sortedDailyTask.length; i++) {
      let statusStyle;
      let mustDoNow = "";
      if (sortedDailyTask[i].status == 1) {
        statusStyle = "on-going";
      } else if (sortedDailyTask[i].status == 2) {
        statusStyle = "completed";
      } else {
        statusStyle = "unfinished";
      }

      let start = convertDate(sortedDailyTask[i].start_time);
      let end = convertDate(sortedDailyTask[i].end_time);

      start = start.getHours() * 60 + start.getMinutes();
      end = end.getHours() * 60 + end.getMinutes();
      nowDate = nowHour * 60 + nowMin;

      if (start <= nowDate && nowDate < end) {
        mustDoNow = "must-do-now";
      }

      $(".daily-tasks").append(
        `
            <div class="daily-tasks-box ${mustDoNow}" id='daily-target-${sortedDailyTask[i].id}'>
            <div class='status-side ${statusStyle}'></div>    
              <div class='right-side'>
                <p class="daily-title">${sortedDailyTask[i].judul}</p>
                <p class="daily-date">${dayOfWeekAsString(
                      sortedDailyTask[i].day
                    )} <br/>
                    ${sortedDailyTask[i].start_time.slice(0,5)}-${sortedDailyTask[i].end_time.slice(0,5)} ${checkNight(sortedDailyTask[i].end_time)}
                </p>
              </div>
            </div>
        `
      );

      $("#put-daily").append(
        `
            <input type="submit" value="Submit" id='button-daily-${sortedDailyTask[i].id}' class='hide-submit btn btn-primary bottom-right'>
        `
        );

        $(`#button-daily-${sortedDailyTask[i].id}`).on("click", (e) => {
            putDaily(e, sortedDailyTask[i].id);
        });


      $(`#daily-target-${sortedDailyTask[i].id}`).on("click",function(e) {
          updateDaily(e,sortedDailyTask[i].id)
      });

    }
  });
};

let checkNight = (time) => {
  var hours = parseInt(time.slice(0,3));
  var minutes = parseInt(time.slice(3,5));
  var ampm = hours >= 12 ? 'PM' : 'AM';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = ampm;
  return strTime;
}

function updateTasks(event, id) {
  $(".bg-modal-2").css({ display: "flex" });
  $(`#button-target-${id}`).removeClass("hide-submit");
  $("#put-form").removeClass("put-target-form");
  $("#put-form").css('display','flex');
  $("#put-form").css('flex-direction','column');

  close(`#button-target-${id}`);


  $.ajax({
    url: `put-targeted-tasks/${id}`,
    success: function (response) {
      $("#target_put_deadlines").val(`${response[1].fields.deadlines}`)
      $("#target_put_judul").val(`${response[0].fields.judul}`);
      $("#target_put_description").val(`${response[0].fields.description}`);
      $("#target_put_status").val(`${response[0].fields.status}`);
    },
    error: (response) => {
      console.log(response);
    },
  });
}

function updateDaily(event,id) {
    $(".bg-modal-2").css({ display: "flex" });
    $(`#button-daily-${id}`).removeClass("hide-submit");
    $("#put-daily").removeClass("put-daily-form");
    $("#put-daily").css("display","flex");
    $("#put-daily").css("flex-direction","column");
    close(`#button-daily-${id}`);

    $.ajax({
      url: `put-daily-tasks/${id}`,
      success: function (response) {
        $("#daily_put_day").val(`${response[1].fields.day}`);
        $("#daily_put_judul").val(`${response[0].fields.judul}`);
        $("#daily_put_start_time").val(`${response[1].fields.start_time}`);
        $("#daily_put_description").val(`${response[0].fields.description}`);
        $("#daily_put_end_time").val(`${response[1].fields.end_time}`);
        $("#daily_put_status").val(`${response[0].fields.status}`);
      },
      error: (response) => {
        console.log(response);
      },
    });
}

putForm = (e, id) => {
  e.preventDefault();
  let csrf = document.getElementsByName("csrfmiddlewaretoken")[0].value;
  const targetStatus = document.getElementById("target_put_status").value;
  data = {
    deadlines: $("#target_put_deadlines").val(),
    judul: $("#target_put_judul").val(),
    description: $("#target_put_description").val(),
    status: targetStatus,
    csrfmiddlewaretoken: csrf,
  };
  $.ajax({
    type: "POST",
    url: `put-targeted-tasks/${id}`,
    data: data,
    success: function (response) {
      $("tbody").html("");
      fetchAlltasks(renderTasks, "All Tasks");
      remainingTasks();
    },
    error: function (response) {
      console.log(response);
    },
  });
  e.stopImmediatePropagation();
  return false;
};

putDaily = (e,id) => {
    e.preventDefault();
    let csrf = document.getElementsByName("csrfmiddlewaretoken")[0].value;
    const targetStatus = document.getElementById("daily_put_status").value;
  
    data = {
      judul: $("#daily_put_judul").val(),
      description: $("#daily_put_description").val(),
      day : $("#daily_put_day").val(),
      start_time : $("#daily_put_start_time").val(),
      end_time : $("#daily_put_end_time").val(),
      status: targetStatus,
      csrfmiddlewaretoken: csrf,
    };


    $.ajax({
      type: "POST",
      url: `put-daily-tasks/${id}`,
      data: data,   
      success: function (response) {
        $(".daily-tasks").html("");
        $(".daily-tasks").append(`
                <p class="anulah">Daily Tasks</p>
        `);
        fetchDailyTasks(renderDailyTasks);
        remainingTasks();
      },
      error: function (response) {
        console.log(response);
      },
    });
    e.stopImmediatePropagation();
    return false;
}

deleteTarget = (e,id) => {
  let csrf = document.getElementsByName("csrfmiddlewaretoken")[0].value;
  data = {'csrfmiddlewaretoken' : csrf}
  $.ajax({
    type: "POST",
    data: data,
    url : `delete-target-tasks/${id}`,
    success: (response) => {
      $("tbody").html("");
      fetchAlltasks(renderTasks, "All Tasks");
      remainingTasks();      
    },
    error: (response) => {
      console.log(response)
    }
  })
}

function dayOfWeekAsString(dayIndex) {
  return [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ][dayIndex];
}

let renderTasks = (response, status) => {
  $(document).ready(() => {
    sortStatusTasks = sortedTask(response, status);
    for (let i = 0; i < sortStatusTasks.length; i++) {
      let status;
      let statusStyle;

      if (sortStatusTasks[i].status == 1) {
        status = "On Going";
        statusStyle = "on-going";
      } else if (sortStatusTasks[i].status == 2) {
        status = "Completed";
        statusStyle = "completed";
      } else {
        status = "Unfinished";
        statusStyle = "unfinished";
      }
      $("tbody").append(
        `
                <tr>
                    <td>                        
                         <div>${sortStatusTasks[i].judul} </div>
                    </td>
                    <td>
                        ${sortStatusTasks[i].deadlines}
                    </td>
                    <td class="status">
                        <p 
                            id="tstatus${sortStatusTasks[i].id}" 
                            class="status-style ${statusStyle}">
                        </p>
                    </td>
                    <td>
                    <div class="dropdown">
                        <i onclick="myFunction(${sortStatusTasks[i].id})" class="dropbtn fas fa-chevron-down"></i>
                      <div id="myDropdown${sortStatusTasks[i].id}" class="dropdown-content">
                        <p id='paraf${sortStatusTasks[i].id}' class="dd-content">Edit</p>
                        <p id='delete-target-${sortStatusTasks[i].id}' class="dd-content"> Delete </p>
                      </div>
                    </div>
                    </td>
                </tr>
                `
      );

      $("#put-form").append(
                `
                <input type="submit" value="Submit" id='button-target-${sortStatusTasks[i].id}' class='hide-submit btn btn-primary bottom-right'>
                `
      );

      $(`#button-target-${sortStatusTasks[i].id}`).on("click", (e) => {
        putForm(e, sortStatusTasks[i].id);
      });

      $(`#paraf${sortStatusTasks[i].id}`).on("click", (e) => {
        updateTasks(e, sortStatusTasks[i].id);
      });

      
      $(`#delete-target-${sortStatusTasks[i].id}`).on("click", (e) => {
        deleteTarget(e, sortStatusTasks[i].id);
      });
    }
  });
};

let sortedTask = (tasks, status) => {
  let sorted_tasks = [];

  for (let i = 0; i < tasks.length; i += 2) {
    let obj = {};
    obj["judul"] = tasks[i].fields.judul;
    obj["status"] = tasks[i].fields.status;
    obj["deadlines"] = tasks[i + 1].fields.deadlines;
    obj["id"] = tasks[i].pk;
    if (status === "All Tasks") sorted_tasks.push(obj);
    else if (status == obj.status) {
      sorted_tasks.push(obj);
    }
  }

  sorted_tasks.sort((a, b) => {
    return new Date(a.deadlines) - new Date(b.deadlines);
  });

  return sorted_tasks;
};

let sortDaily = (tasks) => {
  let sorted_daily = [];
  for (let i = 0; i < tasks.length; i += 2) {
    let obj = {};
    obj['id'] = tasks[i].pk
    obj["judul"] = tasks[i].fields.judul;
    obj["status"] = tasks[i].fields.status;
    obj["start_time"] = tasks[i + 1].fields.start_time;
    obj["end_time"] = tasks[i + 1].fields.end_time;
    obj["day"] = tasks[i + 1].fields.day;
    sorted_daily.push(obj);
  }

  sorted_daily.sort((a, b) => {
    let newA = convertDate(a.start_time);
    let newB = convertDate(b.start_time);
    return newA - newB;
  });

  return sorted_daily;
};

let convertDate = (date) => {
  let newDate = new Date();
  let arr = date.split(":");
  newDate.setHours(arr[0]);
  newDate.setMinutes(arr[1]);
  newDate.setSeconds(arr[2]);
  return newDate;
};

$(".balltasks").click(() => {
  $("button").removeClass("selected");

  $(".balltasks").addClass("selected");
  $("tbody").html("");
  fetchAlltasks(renderTasks, "All Tasks");
});

$(".bunfinished").click(() => {
  $("button").removeClass("selected");

  $(".bunfinished").addClass("selected");
  $("tbody").html("");
  fetchAlltasks(renderTasks, 3);
});

$(".bongoing").click(() => {
  $("button").removeClass("selected");

  $(".bongoing").addClass("selected");
  $("tbody").html("");
  fetchAlltasks(renderTasks, 1);
});

$(".bcompleted").click(() => {
  $("button").removeClass("selected");

  $(".bcompleted").addClass("selected");
  $("tbody").html("");
  fetchAlltasks(renderTasks, 2);
});

function close(id_tag) {
  $(".close-2").click(() => {
    $(".bg-modal-2").css({ display: "none" });
    $(id_tag).addClass("hide-submit");
    $("#put-form").css({'display':'none'})
    $("#put-daily").css({'display' : 'none'});
    $("#put-form").addClass("put-target-form");
    $("#put-daily").addClass("put-daily-form");
  });
}

$(document).ready(() => {
  $(".add-tasks").click(() => {
    $(".bg-modal-1").css({ display: "flex" });
  });

  $('.close-btn').click(function(){
    $('.alert').removeClass("show");
    $('.alert').addClass("hide");
  })

  $(".close-1").click(() => {
    $(".bg-modal-1").css({ display: "none" });
  });

  $(".theader").click(() => {
    $("p").removeClass("selected-header");
    $(".theader").removeClass("unselected-header");
    $(".dheader").addClass("unselected-header");
    $(".theader").addClass("selected-header");
    $("#post-form").css({ display: "none" });
    $("#target-form").css({ display: "flex" });
  });

  $(".dheader").click(() => {
    $("p").removeClass("selected-header");
    $(".dheader").removeClass("unselected-header");
    $(".theader").addClass("unselected-header");
    $(".dheader").addClass("selected-header");
    $("#post-form").css({ display: "flex" });
    $("#target-form").css({ display: "none" });
  });
});

function calendar() {
  date.setDate(1);
  let today = date.getDate();
  let monthDays = document.querySelector(".days-sendiri");
  let lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate();
  let prevLastDay = new Date(date.getFullYear(), date.getMonth(), 0).getDate();

  let lastDayIndex = new Date(
    date.getFullYear(),
    date.getMonth() + 1,
    0
  ).getDay();
  let firstDayIndex = date.getDay();

  const months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  let month = date.getFullYear() + " " + months[date.getMonth()];
  document.querySelector(".month p").innerHTML = month;
  let days = "";

  for (let x = firstDayIndex; x > 0; x--) {
    days += `<div class='prev-date'>${prevLastDay - x + 1}</div>`;
  }
  for (let i = 1; i <= lastDay; i++) {
    if (
      i === new Date().getDate() &&
      date.getMonth() === new Date().getMonth()
    ) {
      days += `<div class='today'> ${i}</div>`;
    } else {
      days += `<div>${i}</div>`;
    }
  }

  for (let i = 1; i <= 7 - lastDayIndex - 1; i++) {
    days += `<div class='next-date'> ${i} </div>`;
  }

  monthDays.innerHTML = days;
}




let targetForm = document.getElementById("target-form");
const targetDeadlines = document.getElementById("target_deadlines");
const targetJudul = document.getElementById("target_judul");
const targetDescription = document.getElementById("target_description");
const targetStatus = document.getElementById("target_status");
const csrfTarget = document.getElementsByName("csrfmiddlewaretoken");

targetForm.addEventListener("submit", (e) => {
  e.preventDefault();
  const fd = new FormData();
  fd.append("csrfmiddlewaretoken", csrfTarget[0].value);
  fd.append("judul", targetJudul.value);
  fd.append("description", targetDescription.value);
  fd.append("status", targetStatus.value);
  fd.append("deadlines", targetDeadlines.value);

  $.ajax({
    type: "POST",
    url: "post-targeted-tasks/",
    data: fd,
    success: (response) => {
    $(".bg-modal-1").css({ display: "none" });
      $("tbody").html("");
      $("#target_deadlines").val('')
      $("#target_judul").val('')
      $("#target_description").val('')
      $("#target_status").val('')
      fetchAlltasks(renderTasks, "All Tasks");
      remainingTasks();
    },
    error: (response) => {
      console.log(response);
    },
    cache: false,
    contentType: false,
    processData: false,
  });
});

let form = document.getElementById("post-form");
const judul = document.getElementById("id_judul");
const deskripsi = document.getElementById("id_description");
const day = document.getElementById("id_day");
const start_time = document.getElementById("id_start_time");
const end_time = document.getElementById("id_end_time");
const statuses = document.getElementById("id_status");
const csrf = document.getElementsByName("csrfmiddlewaretoken");

form.addEventListener("submit", (e) => {
  e.preventDefault();
  const fd = new FormData();
  fd.append("csrfmiddlewaretoken", csrf[0].value);
  fd.append("judul", judul.value);
  fd.append("description", deskripsi.value);
  fd.append("day", day.value);
  fd.append("start_time", start_time.value);
  fd.append("end_time", end_time.value);
  fd.append("status", statuses.value);

  $.ajax({
    type: "POST",
    url: "post-daily-tasks/",
    data: fd,
    success: (response) => {
    $(".bg-modal-1").css({ display: "none" });
      $('#id_judul').val('')
      $('#id_description').val('')
      $('#id_day').val('')
      $('#id_start_time').val('')
      $('#id_end_time').val('')
      $('#id_status').val('')
      $(".daily-tasks").html("");
      $(".daily-tasks").append(`
                <p class="anulah">Daily Tasks</p>
            `);
      fetchDailyTasks(renderDailyTasks);
      remainingTasks();
    },
    error: (response) => {
      console.log(response);
    },
    cache: false,
    contentType: false,
    processData: false,
  });
});

function myFunction(id) {
  document.getElementById(`myDropdown${id}`).classList.toggle("show");
}

// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

fetchDailyTasks(renderDailyTasks);
fetchAlltasks(renderTasks, "All Tasks");
remainingTasks();