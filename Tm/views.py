from django.http.response import JsonResponse
from django.shortcuts import render,get_object_or_404
from django.http import HttpResponse
from .models import TargetedTask,DailyTask,Task
from django.core import serializers
from datetime import date,time
from .forms import *
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt


@login_required(login_url='/admin/login/')        
def testing(request):
    response = {}
    daily_form = DailyTaskForm()
    targeted_form = TargetedTaskForm(auto_id ="target_%s")
    targeted_put_form = TargetedTaskForm(auto_id="target_put_%s")
    daily_put_form = DailyTaskForm(auto_id="daily_put_%s")
    response['daily'] = daily_form
    response['target'] = targeted_form
    response['target_put'] = targeted_put_form 
    response['daily_put'] = daily_put_form
    return render(request,'index.html',response)

def count_tasks(request):
    user = request.user
    if request.user.is_authenticated or "Authorization" in request.headers:
        if "Authorization" in request.headers :
            token = request.headers['Authorization'].split()[1]
            user = User.objects.get(auth_token=token)
            if not user:
                return JsonResponse({"error" : "no user can be found"},404)
        today = (datetime.datetime.today().weekday() + 1)%7
        daily_task = DailyTask.objects.filter(user=user,status=1,day=str(today)).count()
        targeted_task = TargetedTask.objects.filter(user=user,status=1).count()
        return JsonResponse({'counts' : daily_task + targeted_task},status=200)
    return JsonResponse({"error": "user is not logged in "},status=401)

@csrf_exempt
def post_daily_task(request):
    form = DailyTaskForm(request.POST or None)
    data = {}
    user = request.user

    if request.method == "POST" and "Authorization" in request.headers :
        token = request.headers['Authorization'].split()[1]
        user = User.objects.get(auth_token = token)
        if not user :
            return JsonResponse({"error" : "no user can be found"},content_type='application/json',status=401)
        judul = request.POST.get("judul")
        deskripsi = request.POST.get("description")
        day = request.POST.get("day")
        start_time = request.POST.get("start_time")
        end_time = request.POST.get("end_time")
        status = request.POST.get("status")

        new_tasks = DailyTask(
            user=user,
            description = deskripsi,
            day = day,
            start_time=start_time,
            end_time=end_time,
            judul=judul,
            status=status
        )
        new_tasks.save()
        data['status'] = 'ok'
        return JsonResponse(data,status=200)

    elif user.is_authenticated and request.is_ajax():
        if form.is_valid():
            form = form.save(commit=False)
            form.user = user
            form.save()
            data['status'] = 'ok'
            return JsonResponse(data,status=200)
        else:
            return JsonResponse({"error":form.errors},status=400)
    
    return JsonResponse({"error" : "user is not logged in"},status=404)

@csrf_exempt
def post_target_task(request):
    form = TargetedTaskForm(request.POST or None)
    data = {}

    if request.method == "POST" and "Authorization" in request.headers :
        token = request.headers['Authorization'].split()[1]
        user = User.objects.get(auth_token = token)
        if not user :
            return JsonResponse({"error" : "no user can be found"},content_type='application/json',status=401)
        judul = request.POST.get("judul")
        deskripsi = request.POST.get("description")
        deadlines = request.POST.get('deadlines')
        status = request.POST.get("status")

        new_tasks = TargetedTask(
            user=user,
            description = deskripsi,
            judul=judul,
            status = status,
            deadlines=deadlines
        )

        new_tasks.save()
        data['status'] = 'ok'
        return JsonResponse(data,status=200)

    elif request.is_ajax():
        if form.is_valid():
            form = form.save(commit=False)
            form.user = request.user
            form.save()
            data['status'] = 'ok'
            return JsonResponse(data,status=200)
        
    else :
        return JsonResponse({"error": form.errors },status=400)

@csrf_exempt      
def put_target_task(request,id):
    response = {}
    general_tasks = get_object_or_404(Task,id=id)
    target_tasks = get_object_or_404(TargetedTask,id=id)
    print("target mau di update")

    if request.method == "POST" and "Authorization" in request.headers :
        token = request.headers['Authorization'].split()[1]
        user = User.objects.get(auth_token = token)

        if not user :
            return JsonResponse({"error" : "no user can be found"},content_type='application/json',status=401)

        judul = request.POST.get("judul")
        deskripsi = request.POST.get("description")
        deadlines = request.POST.get('deadlines')
        status = request.POST.get("status")

        update_tasks = TargetedTask.objects.get(user=user,pk=id)

        if update_tasks is None:
            return JsonResponse({"error" : "object does not exist"},status=400)

        update_tasks.judul = judul
        update_tasks.description = deskripsi
        update_tasks.status = status
        update_tasks.deadlines = deadlines

        update_tasks.save()
        response['status'] = 'ok'
        return JsonResponse(response,status=200)
    
    elif request.method == "POST" and request.is_ajax():
        form = TargetedTaskForm(request.POST,instance=target_tasks)
        if form.is_valid():
            form = form.save(commit=False)
            form.user = request.user
            form.save()
            response['status'] = 'ok'
            return JsonResponse(response,status=200)
        else:
            return JsonResponse({"error":form.errors},status=400)
    
    else:
        form = TargetedTaskForm()
    
    serialized = serializers.serialize('json',[general_tasks,target_tasks])
    return HttpResponse(serialized,content_type='application/json')

@csrf_exempt
def put_daily_task(request,id):
    response = {}
    general_tasks = get_object_or_404(Task,id=id)
    daily_tasks = get_object_or_404(DailyTask,id=id)

    if request.method == "POST" and "Authorization" in request.headers :
        token = request.headers['Authorization'].split()[1]
        user = User.objects.get(auth_token = token)
        if not user :
            return JsonResponse({"error" : "no user can be found"},content_type='application/json',status=401)

        judul = request.POST.get("judul")
        deskripsi = request.POST.get("description")
        day = request.POST.get("day")
        start_time = request.POST.get("start_time")
        end_time = request.POST.get("end_time")
        status = request.POST.get("status")

        update_tasks = DailyTask.objects.get(user=user,pk=id)

        if update_tasks is None:
            return JsonResponse({"error" : "object does not exist"},status=400)

        update_tasks.judul = judul
        update_tasks.description = deskripsi
        update_tasks.day = day
        update_tasks.start_time = start_time
        update_tasks.end_time = end_time
        update_tasks.status = status

        update_tasks.save()
        response['status'] = 'ok'
        return JsonResponse(response,status=200)
    
    if request.method == "POST" and request.is_ajax():
        form = DailyTaskForm(request.POST,instance=daily_tasks)
        if form.is_valid():
            form = form.save(commit=False)
            form.user = request.user
            form.save()
            response['status'] = 'ok'
            return JsonResponse(response,status=200)
        else:
            return JsonResponse({"error":form.errors},status=400)
    else:
        form = DailyTaskForm()
    
    serialized = serializers.serialize('json',[general_tasks,daily_tasks])
    return HttpResponse(serialized,content_type='application/json')

@csrf_exempt
def delete_target_task(request,id):
    response = {}
    target_task = get_object_or_404(TargetedTask,id=id)
    print("target mau di delete")
    if request.method == "POST":
        target_task.delete()
        response = {'respon': f"tugas dengan id {id} berhasil dihapus"}
        return JsonResponse(response,content_type='application/json',status=200)


@csrf_exempt
def delete_daily_tasks(request,id):
    response = {}
    daily_tasks = get_object_or_404(DailyTask,id=id)
    print("daily mau di delete")
    if request.method == "POST":
        daily_tasks.delete()
        response = {'respon': f"tugas dengan id {id} berhasil dihapus"}
        return JsonResponse(response,content_type='application/json',status=200)
    
def filter_deadline_tasks(query):
    hash_map = {}
    filtered_query = []
    for obj in query:
        if obj.id in hash_map:
            if hasattr(obj,'deadlines') or hasattr(hash_map[obj.id],'deadlines'):
                filtered_query.append(obj)
                filtered_query.append(hash_map[obj.id])
        else :
            hash_map[obj.id] = obj

    return filtered_query

def filter_daily_tasks(query):
    hash_map = {}
    filtered_query = []
    for obj in query:
        if obj.id in hash_map:
            if hasattr(obj,'start_time') or hasattr(hash_map[obj.id],'start_time'):
                filtered_query.append(obj)
                filtered_query.append(hash_map[obj.id])
        else :
            hash_map[obj.id] = obj
    
    return filtered_query

def check_query_status(query):
    for task in query:
        if hasattr(task,'deadlines'):
            if task.status != "2" and task.deadlines < date.today():
                task.status = "3"
            task.save()
        elif hasattr(task,'start_time') and hasattr(task,'end_time'):
            now = datetime.datetime.now()
            current_time = time(now.hour,now.minute,now.second)
            if task.status != "2" and not task.time_in_range(task.start_time,task.end_time,current_time) and current_time >= task.start_time:
                task.status = "3"

            task.save()

def get_daily_tasks(request,id):
    user = request.user
    if not user.is_authenticated and "Authorization" in request.headers:
        token = request.headers["Authorization"].split()[1]
        user = User.objects.get(auth_token = token)
        if not user :
            return JsonResponse({"error" : "no user can be found"},content_type='application/json',status=401)
    elif not user.is_authenticated :
            return JsonResponse({"error" : "user is not logged in"},content_type='application/json',status=401)

    check_query_status(DailyTask.objects.all())
    task_query = [*DailyTask.objects.filter(day=str(id),user=user), *Task.objects.all().filter(user=user)]
    task_query = filter_daily_tasks(task_query)
    serialized_query = serializers.serialize('json',task_query)
    return HttpResponse(serialized_query,content_type='application/json')


def get_deadline_tasks(request):
    user = request.user
    if not user.is_authenticated and "Authorization" in request.headers:
        token = request.headers["Authorization"].split()[1]
        user = User.objects.get(auth_token = token)
        if not user :
            return JsonResponse({"error" : "no user can be found"},content_type='application/json',status=401)
    elif not user.is_authenticated :
            return JsonResponse({"error" : "user is not logged in"},content_type='application/json',status=401)

    check_query_status(TargetedTask.objects.all())
    task_query = [*TargetedTask.objects.all().filter(user=user), *Task.objects.all().filter(user=user)]
    task_query = filter_deadline_tasks(task_query)
    serialized_query  = serializers.serialize('json',task_query)
    return HttpResponse(serialized_query,content_type='application/json')


    