# Generated by Django 3.2.8 on 2021-10-23 23:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Tm', '0004_alter_task_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dailytask',
            name='day',
            field=models.CharField(choices=[('1', 'Monday'), ('2', 'Tuesday'), ('3', 'Wednesday'), ('4', 'Thursday'), ('5', 'Friday'), ('6', 'Saturday'), ('0', 'Sunday')], max_length=9),
        ),
    ]
