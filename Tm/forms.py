from django.forms import ModelForm,TextInput, widgets,Textarea,Select,DateInput
from .models import *
import datetime
from django import forms

class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = '__all__'
        exclude = ('user',)
        widgets =  {
        'judul': TextInput({
            'class' : 'form-control',
            'style' : 'max-width: 300px',
            'placeholder' : 'Judul'
        }),
        'description': Textarea({
            'class' : 'form-control text-area',
            'style' : 'max-width: 300px',
            'placeholder' : 'Dekripsi'
        }),
        'status' : Select({
            'class' : 'form-control',
        }),

    }

class DailyTaskForm(TaskForm):
    start_time = forms.TimeField(
        label='Waktu Awal',
        widget= forms.TimeInput({'class': 'form-control','type': 'time'}),
    )
    end_time = forms.TimeField(
        label='Waktu Akhir',
        widget= forms.TimeInput({'class': 'form-control','type': 'time'}),
    )

    def __init__(self,*args,**kwargs):
        super(DailyTaskForm,self).__init__(*args,**kwargs)

    class Meta(TaskForm.Meta):
        model = DailyTask
        fields = TaskForm.Meta.fields 
        widgets = TaskForm.Meta.widgets
        widgets['day'] = Select({'class': 'form-control'})

class TargetedTaskForm(TaskForm):
    def __init__(self,*args,**kwargs): 
        super(TargetedTaskForm,self).__init__(*args,**kwargs)

    deadlines = forms.DateField(
            label='Waktu Awal',
            widget= forms.DateInput({'class': 'form-control','type': 'date'})
        )
    class Meta(TaskForm.Meta):  
     
        model = TargetedTask
        fields = TaskForm.Meta.fields
        widgets =  TaskForm.Meta.widgets

        