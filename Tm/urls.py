from django.urls import path
from . import views

urlpatterns = [
    path('', views.testing,name='tm-index'),
    path('get_tasks/',views.get_deadline_tasks,name='get-tasks'),
    path('get_daily_tasks/<int:id>',views.get_daily_tasks,name='daily-tasks'),
    path('post-daily-tasks/',views.post_daily_task,name='post-daily'),
    path('post-targeted-tasks/',views.post_target_task,name='post-target'),
    path('put-targeted-tasks/<int:id>',views.put_target_task,name='put-target'),
    path('put-daily-tasks/<int:id>',views.put_daily_task,name='put-daily'),
    path('delete-target-tasks/<int:id>',views.delete_target_task,name='delete-target'),
    path('remaining-tasks/',views.count_tasks,name='count-tasks'),
    path('delete-daily-tasks/<int:id>',views.delete_daily_tasks,name='delete-daily-tasks')
]