from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.core import serializers
from django.http.response import HttpResponse

from .models import Bookmark, Tag
from .forms import BookmarkForm, TagForm


@login_required(login_url='/admin/login/')
def bookmark_index(request):
    bookmarks = Bookmark.objects.all().filter(user=request.user)
    tags = Tag.objects.all().filter(user=request.user)

    form = BookmarkForm(request.POST)

    if request.method == 'POST' and form.is_valid():
        form.instance.user = request.user # Fix
        form = form.save()
        form.user = request.user
        form.save()

        return redirect('/bookmarks')

    response = {
        'bookmarks': bookmarks,
        'bookmark_count': len(bookmarks),
        'form': form,
        'user': request.user,
        'tags': tags,
    }

    return render(request, 'bookmark_index.html', response)


@login_required(login_url='/admin/login')
def add_tag(request):
    form = TagForm(request.POST)

    if request.method == 'POST' and form.is_valid():
        form.instance.user = request.user
        form = form.save()
        form.user = request.user
        form.save()

    return redirect('/bookmarks')


@login_required(login_url='/admin/login/')
def delete_bookmark(request, id):
    get_object_or_404(Bookmark, id=id).delete()
    return redirect('/bookmarks')


@login_required(login_url='admin/login')
def filter_bookmarks(request, tag):
    data = serializers.serialize('json', Bookmark.objects.all().filter(user=request.user).filter(tags__title__icontains=tag))
    return HttpResponse(data, content_type="application/json")


@login_required(login_url='admin/login')
def search_bookmarks(request, query):
    data = serializers.serialize('json', Bookmark.objects.all().filter(user=request.user).filter(title__icontains=query))
    return HttpResponse(data, content_type="application/json")


@login_required(login_url='admin/login')
def get_bookmarks(request):
    bookmarks = serializers.serialize('json', Bookmark.objects.filter(user=request.user))
    return HttpResponse(bookmarks, content_type="application/json")
