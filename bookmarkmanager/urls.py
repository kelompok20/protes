from django.urls import path
import bookmarkmanager.views as views

urlpatterns = [
    path('', views.bookmark_index, name='bookmark_index'),
    path('delete/<int:id>', views.delete_bookmark, name='delete_bookmark'),
    path('filter/<str:tag>', views.filter_bookmarks, name='filter_bookmarks'),
    path('search/<str:query>', views.search_bookmarks, name='search_bookmarks'),
    path('add-tag', views.add_tag, name='add_tag'),
    path('get-bookmarks', views.get_bookmarks, name='get_bookmarks')
]