from django import forms
from .models import Tag, Bookmark


class BookmarkForm(forms.ModelForm):
    class Meta:
        model = Bookmark
        fields = [
            'title',
            'description',
            'url',
            'tags',
        ]


class TagForm(forms.ModelForm):
    class Meta:
        model = Tag
        fields = [
            'title',
        ]
