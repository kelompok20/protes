from django.db import models
from django.contrib.auth.models import User

from datetime import datetime


class Tag(models.Model):
    """
    Tags to add to Bookmarks.
    """
    title = models.CharField(max_length=30, unique=True)

    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.title


class Bookmark(models.Model):
    """
    A bookmark entry for links.
    """
    title = models.CharField(max_length=30)
    description = models.CharField(max_length=100)
    url = models.URLField()

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    time_added = models.DateTimeField(default=datetime.now)

    tags = models.ManyToManyField(Tag)

    class Meta:
        ordering = ('-time_added',)

    def __str__(self):
        return self.title
