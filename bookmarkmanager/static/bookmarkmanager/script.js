const filterBtn = document.getElementById("filter-button");
const searchBtn = document.getElementById("search-button");

searchBtn.addEventListener("click", function() {
    const query = document.getElementById("query").value;
    const request = new XMLHttpRequest();
    request.open('GET', "search/"+query);

    request.onload = function() {
        if (request.status >= 200 && request.status < 400) {
            const data = JSON.parse(request.responseText);
            // Process data
        } else {
            console.log("Server error!");
        }
    };

    request.onerror = function() {
        console.log("Connection error!");
    };

    request.send();
});

filterBtn.addEventListener("click", function(){
    const diff = document.getElementById("tags").value;
    const request = new XMLHttpRequest();
    request.open('GET', diff);

    request.onload = function() {
        if (request.status >= 200 && request.status < 400) {
            const data = JSON.parse(request.responseText);
            // Process data
        } else {
            console.log("Server error!");
        }
    };

    request.onerror = function() {
        console.log("Connection error!");
    };

    request.send();
});

document.querySelector('.close').addEventListener("click", function() {
	document.querySelector('.bg-modal').style.display = "none";
});

const deletes = document.getElementsByName("delete");
deletes.forEach(function(del){
    del.addEventListener("click", func);
});

function func(){
    document.getElementById('add-tag-form').style.display = "none";
    document.getElementById('add-form').style.display = "none";
    document.getElementById('delete-prompt').style.display = "block";
    document.getElementById('delete-prompt').setAttribute("value", this.getAttribute("value"));
    document.querySelector('.bg-modal').style.display = "block";
}

document.getElementById("delete-bookmark").addEventListener("click", function() {
    const url = document.getElementById("delete-prompt").getAttribute("value");
    window.location.replace(url);
});

document.getElementById('add').addEventListener("click", function() {
    document.getElementById('delete-prompt').style.display = "none";
	document.querySelector('.bg-modal').style.display = "block";
    document.getElementById('add-tag-form').style.display = "none";
    document.getElementById('add-form').style.display = "block";
});

document.getElementById('add-tag').addEventListener("click", function() {
    document.getElementById('delete-prompt').style.display = "none";
	document.querySelector('.bg-modal').style.display = "block";
    document.getElementById('add-form').style.display = "none";
    document.getElementById('add-tag-form').style.display = "block";
});

