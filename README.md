[![coverage report](https://gitlab.com/kelompok20/protes/badges/master/coverage.svg)](https://gitlab.com/kelompok20/protes/-/commits/master)
[![pipeline status](https://gitlab.com/kelompok20/protes/badges/master/pipeline.svg)](https://gitlab.com/kelompok20/protes/-/commits/master)

# Daftar Isi
- [Protes](#protes)
- [Anggota](#anggota-kelompok-e06)
- [Link Heroku](#link-heroku-app)
- [Daftar Modul](#daftar-modul-yang-akan-dikerjakan)
- [Persona](#persona)
- [Gitlab Workflow](#gitlab-workflow)


# Protes (Projek Tengah Semester)
Di masa pandemi ini, sekarang segala hal dilakukan dengan online. Mulai dari belajar, bekerja, hingga berkomunikasi. Banyaknya kegiatan yang harus dilakukan perlu di simpan supaya mudah untuk melacak kegiatan sehari-hari. Berhubung sekarang serba online, akan lebih mudah untuk menyimpan kegiatan yang kita lakukan di suatu website. Oleh karena itu, kami memutuskan untuk membuat suatu web-app yang bertema produktivitas. Di dalam web-app ini, kami akan membuat modul-modul yang mudah untuk melacak produktivitas atau kegiatan sehari-hari sehingga kegiatan di masa pandemi ini lebih terarah.  

# Anggota Kelompok E06
1. Serima Nuranisa Malonda (2006596453)
1. Mohamad Fariz Kurniawan (2006596081)
1. Ikramullah (2006596094)
1. Muhammad Naufal Al Mujaddid Akbar (2006596390)
1. Muhammad Imam Chaidar Mujaddid Al-g (2006596365)
1. Figo Athallah (2006535571)

# Link Heroku App
https://projek-uts.herokuapp.com/


# Daftar modul yang akan dikerjakan


### Sign up (Mohamad Fariz Kurniawan)
Page dimana user dapat membuat akun.

### Log In (Muhammad Imam Chaidar Mujaddid Al-g)
Page dimana user dapat memasukkan id-nya dan password agar dapat log in.

### Task Manager (Ikramullah)
Page yang akan menampilkan tugas-tugas yang harus dilakukan hari ini dan untuk kedepannya. Pengguna juga dapat menambahkan,menghapus, dan mengedit tugas-tugas yang akan dilakukan pada tanggal tertentu. 

### Recipe Manager (Serima Nuranisa Malonda)
Fitur ini dapat digunakan untuk mengumpulkan resep-resep makanan dari  berbagai website, menandai resep itu sebagai easy, medium, atau hard, serta ada fitur search dan filter by difficulty.

### Personal Journal (Figo Athallah)
Fitur dimana user dapat menulis tentang aktivitas mereka sehari-hari dalam semacam jurnal digital. user dapat menulis target/goals yang mereka ingin capai dalam jangka waktu yang mereka tentukan, introspeksi diri, dan quote of the day.

### Bookmark Manager (Muhammad Naufal Al Mujaddid Akbar)
Fitur yang akan menyimpan link website yang berguna. Bookmark ini dapat diberikan tag untuk pencarian link yang lebih mudah. Pengguna bisa melakukan search sesuai judul atau difilter sesuai tag bookmark. Contohnya bookmark dengan link tutorial Django dapat diberikan tag "python" dan "django". Pengguna dapat menambahkan, mengedit, dan menghapus bookmark dan tag

# Persona
1. Guest, pengguna yang belum log in dan harus sign up atau log in untuk menggunakan website secara keseluruhan
2. User, pengguna yang telah log in dan dapat menggunakan fitur-fitur utama dari website
3. Admin, dapat mengelola data di web. 


# First Timer Must Do
1. Abis di clone reponya, jangan lupa buat dulu virtual environment terus pip install -r requirements.txt
2. Kalian bisa baca info tambahan di https://gitlab.com/laymonage/lulusppw.


# Gitlab workflow
Haloo :D bagian ini ditujukan untuk anggota kelompok karena bagian ini bakalan berguna untuk pengerjaan modul nanti. Workflow disini artinya cara kita nanti bakalan ngerjain bagian-bagian dari aplikasi yang bakalan kita buat, nah stepnya sederhana

1. Pertama clone dulu reponya (kalian bisa klik clone, bebas dengan ssh/https, step ini cuman dilakukan sekali)

<pre>git clone https://gitlab.com/kelompok20/project-tengah-semester.git (dengan https) </pre>


2. Setelah clone repo-nya, kita dapat membuat branch baru untuk memudahkan pekerjaan yang kita lakukan

<pre> git checkout -b "nama branch (biasanya fungsionalitas yang ingin kita tambah)" </pre>

3. Setelah kita yakin dengan kode kita, kita dapat push branch tersebut ke repo dengan menggunakan 4 mantra di git
<pre>
 git status (check perubahan yang telah kita lakukan)
 git add -A (menambah semua perubahan yang telah kita buat)
 git commit -m "comment tentang perubahan" (bisa dianggap kayak ctrl-c)
 git push origin "nama branch yang udah dibikin" (tanpa tanda petik)
</pre>

4. Nanti kalian bisa bikin merge request, jadi merge request ini intinya gabungin kode yang udah kita bikin (di branch kita sebelumnya) ke branch utama. Nanti teman-teman bisa saling review dari kodingan yang udah kita buat :D, kalau ada bug dan sejenisnya, kita bisa develop lagi dari branch yang udah kita bikin dan repeat proses dari 3.


## Tips pengembangan

1. Mulai sekarang, **biasakan** untuk me-*reload* *browser* kamu dengan
   <kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>R</kbd> (beberapa *browser* juga bisa
   dengan <kbd>Shift</kbd>+<kbd>F5</kbd>), bukan hanya
   <kbd>Ctrl</kbd>+<kbd>R</kbd> atau <kbd>F5</kbd>. Jika tidak, ketika kamu
   mengembangkan web secara lokal, kamu mungkin akan sering menemukan kasus di
   mana berkas *static* yang muncul di *browser* tidak berubah setelah kamu
   memodifikasinya. Ini terjadi karena *browser* membuat *cache* dari berkas
   *static* kamu supaya dapat diakses lebih cepat. Dengan menekan tombol
   <kbd>Shift</kbd>, *browser* akan [mem-*bypass* berkas *static* yang sudah
   di-*cache*][bypass-cache].

2. Biasakan untuk menulis tes yang berkualitas untuk proyek web kamu, terutama
   untuk logika-logika seperti yang ada pada bagian *views*. Membuat tes akan
   memudahkan kamu untuk menemukan *bug* pada program kamu lebih awal. Selain
   itu, tes juga dapat mencegah kamu menciptakan *bug* baru ketika
   mengembangkan proyek web kamu.
   
3. *Code coverage* 100% tidak menjamin proyek kamu bebas *bug*. Namun, memiliki
   tes yang menguji proyek kamu secara keseluruhan tentu akan sangat
   bermanfaat. Mulailah menulis tes setiap kali kamu membuat fungsionalitas
   baru agar *code coverage* tetap terjaga.
4. Templat ini sengaja tidak menyertakan konfigurasi untuk *linting* seperti
   menggunakan [`flake8`][flake8] atau [`pylint`][pylint]. Hal ini untuk
   mencegah munculnya *warning* pada GitHub Actions atau GitLab CI jika kode
   kamu tidak sesuai aturan-aturan yang diterapkan *linter* tersebut. Jika kamu
   tidak ingin repot mengatur kode kamu supaya rapi, silakan coba
   [`black`][black] dan [`isort`][isort]. Akan sangat baik apabila kamu juga
   membuat konfigurasi untuk menjalankan *linter* tersebut di GitHub Actions
   atau GitLab CI untuk proyek yang kamu buat.
